const problem2 = function (string) {
    if (typeof string == "string") {
        let arrOfString = string.split('.');
        let arr = arrOfString.map(
            val => parseInt(val));
        if (arr.length != 4) {
            return [];
        } else {
            return arr;
        }
    } else {
        return 0;
    }
}
module.exports = problem2;