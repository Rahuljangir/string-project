function problem4(obj) {
    let firstName = (obj.first_name).toLowerCase();
    let lastName = (obj.last_name).toLowerCase();
    let fullName;
    if (obj.middle_name != undefined) {
        let middleName = (obj.middle_name).toLowerCase();
        fullName = `${firstName.charAt(0).toUpperCase() + firstName.substr(1)} ${middleName.charAt(0).toUpperCase() + middleName.substr(1)} ${lastName.charAt(0).toUpperCase() + lastName.substr(1)} `;
    } else {
        fullName = `${firstName.charAt(0).toUpperCase() + firstName.substr(1)} ${lastName.charAt(0).toUpperCase() + lastName.substr(1)} `;
    }

    return fullName;
}
module.exports = problem4;