function problem5(arr) {
    if (arr.length != 0) {
        let string = arr.toString();
        return string.replace(/,/g, ' ');
    } else {
        return '\0'
    }
}
module.exports = problem5;