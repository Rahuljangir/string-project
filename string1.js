const problem1 = function(string){
    if(typeof string == 'string'){
        let str = string.replace('$','')
        .replace(',','');
        if(isNaN(str)){
            return 0;
        }else{
            return parseFloat(str);
        }
    }else{
        return 0;
    }
}
module.exports = problem1;